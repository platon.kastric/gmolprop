import wandb
import json

import torch
from torch_geometric.loader import DataLoader
from torch_geometric.data import InMemoryDataset

import utils.data_transform
import utils.utils

from abc import ABC


class PyGDataModule(ABC):
    raw_dataset: InMemoryDataset = None
    processed_dataset: InMemoryDataset = None

    train_dataset: InMemoryDataset = None
    val_dataset: InMemoryDataset = None
    test_dataset: InMemoryDataset = None

    def __init__(self, dataset, config, val_dataset=None, test_dataset=None) -> None:
        super().__init__()
        self.config = config.Data
        self.raw_dataset = dataset
        self.raw_val_dataset = val_dataset
        self.raw_test_dataset = test_dataset

        # Follow batch
        self.follow_batch = [f"x_{i}" for i in range(len(self.config.smiles_columns))]

        self.__mask_targets()
        self.num_targets = self.raw_dataset.data.y[0].shape[0]
        self.processed_dataset = self.__transform_targets(self.raw_dataset.copy())
        if val_dataset: 
            self.val_dataset = self.__transform_targets(val_dataset.copy())
            if self.raw_dataset.mol_features != self.raw_val_dataset.mol_features:
                raise ValueError(f"Mol features of train and val set differ.")
        if test_dataset:
            self.test_dataset = self.__transform_targets(test_dataset.copy())
            if self.raw_dataset.mol_features != self.raw_test_dataset.mol_features:
                raise ValueError(f"Mol features of train and test set differ.")
        self.mol_features = self.raw_dataset.mol_features

        # Data split
        self.processed_dataset = self.processed_dataset.shuffle() # TODO: test if this works
        if (val_dataset is not None) and (test_dataset is not None):
            self.train_dataset = self.processed_dataset
            pass
        elif (val_dataset is None) and (test_dataset is None):
            sum_fractions = self.config.train_fraction + self.config.val_fraction + self.config.test_fraction
            if sum_fractions != 1: raise ValueError (f"Sum of data split fraction is not 1 ({sum_fractions})")
            self.train_dataset, self.val_dataset, self.test_dataset = utils.data_transform.split_data(
                dataset=self.processed_dataset, 
                train_fraction=self.config.train_fraction, 
                vali_fraction=self.config.val_fraction, 
                test_fraction=self.config.test_fraction
                )
        elif (val_dataset is None):
            sum_fractions = self.config.train_fraction + self.config.val_fraction 
            if sum_fractions != 1: raise ValueError (f"Sum of data split fraction is not 1 ({sum_fractions})")
            self.train_dataset, self.val_dataset, _ = utils.data_transform.split_data(
                dataset=self.processed_dataset, 
                train_fraction=self.config.train_fraction, 
                vali_fraction=self.config.val_fraction, 
                test_fraction=0
                )
        elif (test_dataset is None):
            print(f"Warning: Validation set but not test set is defined. Training set is randomly splitted into training and test set.")
            sum_fractions = self.config.train_fraction + self.config.test_fraction 
            if sum_fractions != 1: raise ValueError (f"Sum of data split fraction is not 1 ({sum_fractions})")
            self.train_dataset, _, self.test_dataset = utils.data_transform.split_data(
                dataset=self.processed_dataset, 
                train_fraction=self.config.train_fraction, 
                vali_fraction=self.config.val_fraction, 
                test_fraction=0
                )

        # Data statistics
        if self.config.task_type == 'regression':
            train_stats = self.__calculate_regression_stats(self.train_dataset, scores=["mean", "std", "min", "max"])
            val_stats = self.__calculate_regression_stats(self.val_dataset, scores=["mean", "std", "min", "max"])
            if self.test_dataset:
                test_stats = self.__calculate_regression_stats(self.test_dataset, scores=["mean", "std", "min", "max"])
            else:
                test_stats = None
        elif self.config.task_type == 'classification':
            train_stats = self.__calculate_classification_stats(self.train_dataset, scores=["class_distribution"]) 
            val_stats = self.__calculate_classification_stats(self.val_dataset)
            if self.test_dataset:
                test_stats = self.__calculate_classification_stats(self.test_dataset)
            else:
                test_stats = None
        self.data_statistics = {
            'Train': train_stats,
            'Val': val_stats,
            'Test': test_stats,
        }


        # Normalization
        # Note that for classification target data transformation is not expected
        self.mean_for_norm = torch.zeros(self.num_targets)
        self.std_for_norm = torch.ones(self.num_targets)
        if self.config.task_type == 'regression':
            for target_idx, target_transf in enumerate(self.config.targets_transformation):
                if target_transf is not None:
                    if "norm" in target_transf: 
                        # Data should be normalized on train data statistics only
                        if self.config.mean_for_norm is not None:
                            self.mean_for_norm[target_idx] = self.config.mean_for_norm[target_idx]
                        else:
                            self.mean_for_norm[target_idx] = self.data_statistics["Train"]["mean"][target_idx]
                        
                        if self.config.std_for_norm is not None:
                            self.std_for_norm[target_idx] = self.config.std_for_norm[target_idx]
                        else:
                            self.std_for_norm[target_idx] = self.data_statistics["Train"]["std"][target_idx]

            self.__normalize_targets()

        # Data info
        self.num_node_features = self.train_dataset.data.x_0[0].shape[0]
        self.num_edge_features = self.train_dataset.data.edge_attr_0[0].shape[0]
        self.num_targets = self.train_dataset.data.y[0].shape[0]

        if config.verbose:
            print(f"\n---> Dataset from {self.config.path}")
            if config.Data.val_path: 
                print(f"---> Validation dataset from {self.config.val_path}")
            if config.Data.test_path: 
                print(f"---> Test dataset from {self.config.test_path}")
            print(f"\n---> Train on targets: {self.config.targets_to_train}")
            if config.task_type == "regression":
                print(f"---> Mean (STD) used for normalization: {self.mean_for_norm} ({self.std_for_norm})")
            train_set_len = len(self.train_dataset)
            val_set_len = len(self.val_dataset)
            test_set_len = len(self.test_dataset) if self.test_dataset is not None else 0
            print(f"---> Training data #: {train_set_len}, Validation data #: {val_set_len}, Test set: {test_set_len} (random seed - {config.random_seed})") 
            print(f"---> Training is based on {str(self.num_node_features)} atom features and {str(self.num_edge_features)} edge features for a molecule.")
            torch.set_printoptions(profile="full")
            #print(f"---> Active node features indices: {torch.max(self.train_dataset.data.x,dim=0).values.tolist()}")
            #print(f"---> Active edge features indices: {torch.max(self.train_dataset.data.edge_attr, dim=0).values.tolist()}")
            print(f"\n---> Train data stats: ")
            print(json.dumps(self.data_statistics['Train'], indent=4, default=utils.utils.json_convert_tensor_tolist))
            print(f"---> Validation data stats: ")
            print(json.dumps(self.data_statistics['Val'], indent=4, default=utils.utils.json_convert_tensor_tolist))
            print(f"---> Test data stats: ")
            print(json.dumps(self.data_statistics['Test'], indent=4, default=utils.utils.json_convert_tensor_tolist))
            print("\n... finished loading of dataset and data preprocessing.")

    def update_config_w_data_features(self, config):
        config.Model.num_input_smiles = len(self.config["smiles_columns"])
        config.Model.num_node_features = self.num_node_features
        config.Model.num_edge_features = self.num_edge_features
        config.Model.input_featurization_atom = self.mol_features["atom"].to_yaml()
        config.Model.input_featurization_bond = self.mol_features["bond"].to_yaml()
        if any(self.mean_for_norm != torch.zeros(self.mean_for_norm.shape[0])) or any(self.std_for_norm != torch.ones(self.std_for_norm.shape[0])) != 1:
            config.Model.output_norm_mean = self.mean_for_norm.tolist()
            config.Model.output_norm_std = self.std_for_norm.tolist()
        return config

    def __mask_targets(self):
        # Select target columns to be trained on
        if (self.config.targets_to_train is not None) and (self.config.targets_to_train != self.config.target_columns):
            target_mask = []
            for target_v in self.config.target_columns:
                if target_v in self.config.targets_to_train: 
                    target_mask.append(True)
                else:
                    target_mask.append(False)
            self.raw_dataset.data.y = self.raw_dataset.data.y[:, target_mask]

    def __transform_targets(self, dataset):
        # General data transformation
        if self.config.targets_transformation is not None:
            for target_idx, target_transf in enumerate(self.config.targets_transformation):
                if target_transf is not None:
                    if "log" in target_transf: 
                        dataset.data.y[:, target_idx] = utils.data_transform.log_transform(self.dataset.data.y[:, target_idx])
                        self.config.targets_to_train[target_idx] += "-log"
        return dataset

    def __calculate_regression_stats(self, dataset, scores=["mean", "std"]):
        stats_dict = {}
        for target_idx in range(self.num_targets):
            target_i_data_y = dataset.data.y[:, target_idx]
            # Filter data (if data point is not avilable in real data, it should have "inf" as value)
            if "mean" in scores:
                if target_idx == 0: stats_dict["mean"] = torch.tensor([])
                stats_dict["mean"] = torch.cat((stats_dict["mean"], torch.mean(target_i_data_y[torch.isfinite(target_i_data_y)]).view(-1)))
            if "std" in scores:
                if target_idx == 0: stats_dict["std"] = torch.tensor([])
                stats_dict["std"] = torch.cat((stats_dict["std"], torch.std(target_i_data_y[torch.isfinite(target_i_data_y)]).view(-1)))
            if "min" in scores:
                if target_idx == 0: stats_dict["min"] = torch.tensor([])
                stats_dict["min"] = torch.cat((stats_dict["min"], torch.min(target_i_data_y[torch.isfinite(target_i_data_y)]).view(-1)))
            if "max" in scores:
                if target_idx == 0: stats_dict["max"] = torch.tensor([])
                stats_dict["max"] = torch.cat((stats_dict["max"], torch.max(target_i_data_y[torch.isfinite(target_i_data_y)]).view(-1)))

        return stats_dict

    def __calculate_classification_stats(self, dataset, scores=["class_distribution"]):
        stats_dict = {}
        for target_idx in range(self.num_targets):
            if "class_distribution" in scores:
                if target_idx == 0: stats_dict["class_distribution"] = []
                # iterate through all classes
                for class_v in range(int(torch.max(dataset.data.y[:, target_idx]).item())+1):
                    if class_v == 0: stats_dict["class_distribution"].append(torch.tensor([]))
                    stats_dict["class_distribution"][target_idx] = torch.cat(( stats_dict["class_distribution"][target_idx], torch.sum(dataset.data.y[:, target_idx][torch.isfinite(dataset.data.y[:, target_idx])] == class_v).view(-1)))

        return stats_dict

    
    def __normalize_targets(self):
        # Specific data transformation (Standardization should not include target values from test set)
            for target_idx, target_transf in enumerate(self.config.targets_transformation):
                if target_transf is not None:
                    if "norm" in target_transf: 
                        self.train_dataset.data.y[:, target_idx] = utils.data_transform.standardize(self.train_dataset.data.y[:, target_idx], self.mean_for_norm[target_idx], self.std_for_norm[target_idx])
                        self.val_dataset.data.y[:, target_idx] = utils.data_transform.standardize(self.val_dataset.data.y[:, target_idx], self.mean_for_norm[target_idx], self.std_for_norm[target_idx])
                        if self.test_dataset:
                            self.test_dataset.data.y[:, target_idx] = utils.data_transform.standardize(self.test_dataset.data.y[:, target_idx], self.mean_for_norm[target_idx], self.std_for_norm[target_idx])

    def get_raw_dataloader(self, batch_size=64):
        return DataLoader(self.raw_dataset, batch_size=batch_size, follow_batch=self.follow_batch)

    def get_train_dataloader(self, batch_size=64, seed=None, shuffle=True):
        if shuffle == True and seed is None:
            print("Warning: Train dataloader is shuffled but not seed provided.")
        return DataLoader(self.train_dataset, batch_size=batch_size, shuffle=shuffle, num_workers=0, pin_memory=True, worker_init_fn=seed, follow_batch=self.follow_batch)

    def get_val_dataloader(self, batch_size=64):
        if self.val_dataset is None:
            return None
        return DataLoader(self.val_dataset, batch_size=batch_size, follow_batch=self.follow_batch)

    def get_test_dataloader(self, batch_size=64):
        if self.test_dataset is None:
            return None
        return DataLoader(self.test_dataset, batch_size=batch_size, follow_batch=self.follow_batch)