import os
import pandas as pd
import json

import torch

import utils.data_transform as data_transform
import utils.analyze_results 
import utils.utils

# Inference
def infer(model, loader, args, device, mean_for_norm=0, std_for_norm=1, get_real_values=False):
    model.to(device)
    model.eval()
    smiles = [[] for _ in range(len(args.Data.smiles_columns))]
    extra_inputs = None
    if not (args.Data.extra_input_columns is None):
        extra_inputs = torch.tensor([], device=device)
    mol_ids = []
    raw_predictions = torch.tensor([], device=device)
    raw_real_values = torch.tensor([], device=device)
    # get all predictions
    for data in loader:
        data = data.to(device)
        mol_ids += data.idx
        for i in range(len(smiles)):
            smiles[i] += data[f"smiles_mol_{i}"]
        if not (args.Data.extra_input_columns is None):
            extra_inputs = torch.cat((extra_inputs, data.graph_features))
        raw_predictions = torch.cat((raw_predictions, model(data).detach()))
        if get_real_values:
            raw_real_values = torch.cat((raw_real_values, data.y))
    
    if args.task_type == 'regression':
        predictions = data_transform.reverse_standardize(raw_predictions, mean_for_norm.to(device), std_for_norm.to(device))
        if get_real_values:
            real_values = data_transform.reverse_standardize(raw_real_values, mean_for_norm.to(device), std_for_norm.to(device))
    elif args.task_type == 'classification':
        # note that raw_predictions correspond to class probabilities
        # raw_real_values is artifact from regression; for classification they just correspond to the real classes
        real_values = raw_real_values
        raw_real_values = None
        # binary case with sigmoid and one output
        if args.Model.num_classes == 2:
            predictions = torch.where(raw_predictions > args.Model.binary_class_threshold, 1, 0)
        # accounting for softmax
        else:
            predictions = raw_predictions.argmax(dim=-1)
    

    raw_predictions = raw_predictions.to('cpu')
    if extra_inputs is not None:
        extra_inputs = extra_inputs.to('cpu')
    predictions = predictions.to('cpu')

    if get_real_values:
        if raw_real_values is not None:
            raw_real_values = raw_real_values.to('cpu')
        real_values = real_values.to('cpu')
    else:
        raw_real_values, real_values = None, None
    
    return mol_ids, smiles, extra_inputs, raw_real_values, raw_predictions, real_values, predictions


# Validation metric calculation
def eval(model, loader, args, device, scores=None, mean_for_norm=None, std_for_norm=None, keep_infer_results=False):
    mol_ids, smiles, extra_inputs, raw_real_values, raw_predictions, real_values, predictions = infer(
        model=model,
        loader=loader,
        args=args,
        device=device,
        mean_for_norm=mean_for_norm,
        std_for_norm=std_for_norm,
        get_real_values=True,
        )

    if args.task_type == 'regression':
        # default regression score
        if scores == None:
            scores = ["mae", "mse", "rmse", "mape", "r2", "maxe", "expl_var"]
        metric_epoch = utils.analyze_results.calculate_regression_metrics(y_true=real_values, y_pred=predictions, scores=scores) 
        if any(mean_for_norm != torch.zeros(mean_for_norm.shape[0])) or any(std_for_norm != torch.ones(std_for_norm.shape[0])) != 1:
            raw_metric_epoch = utils.analyze_results.calculate_regression_metrics(y_true=raw_real_values, y_pred=raw_predictions, scores=scores) 
        else:
            raw_metric_epoch = metric_epoch
    elif args.task_type == 'classification':
        if scores == None:
            scores = ["log_loss", "roc_auc", "balanced_accuracy", "confusion_matrix", "sensitivity", "specificity"]
        metric_epoch = utils.analyze_results.calculate_classification_metrics(y_true=real_values, y_pred=predictions, y_pred_probability=raw_predictions, scores=scores)
        raw_metric_epoch = metric_epoch
    if keep_infer_results:
        return raw_metric_epoch, metric_epoch, mol_ids, smiles, extra_inputs, raw_real_values, raw_predictions, real_values, predictions
    return raw_metric_epoch, metric_epoch


def infer_ensemble(model_list, cfg, dataloader=None, dataloader_list=None):
    # Note that dataloader_list assumes same order of inputs
    if (dataloader is None) and (dataloader_list is None):
        raise ValueError("Please provide dataloader.")
    prediction_all = []
    raw_prediction_all = []
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    for i, model_i in enumerate(model_list):
        if not (dataloader_list is None):
            dataloader = dataloader_list[i]
        _, _, _, _, raw_predictions, _, predictions = infer(
            model=model_i,
            loader = dataloader,
            args = cfg,
            device = device,
            mean_for_norm = torch.tensor(model_i.output_norm_mean, dtype=float),
            std_for_norm = torch.tensor(model_i.output_norm_std, dtype=float)
            )
        prediction_all.append(predictions)
        raw_prediction_all.append(raw_predictions)
    prediction_all = torch.stack(prediction_all)
    raw_prediction_all = torch.stack(raw_prediction_all)
    if cfg.task_type == "regression":
        mean_predictions = prediction_all.mean(dim=0)
        std_predictions = prediction_all.std(dim=0)
        return mean_predictions, std_predictions, None, None
    elif cfg.task_type == "classification":
        mean_raw_predictions = raw_prediction_all.mean(dim=0)
        std_raw_preditions = raw_prediction_all.std(dim=0)
        # binary case with sigmoid and one output
        if cfg.Model.num_classes == 2:
            mean_predictions = torch.where(mean_raw_predictions > cfg.Model.binary_class_threshold, 1, 0)
        # accounting for softmax
        else:
            mean_predictions = mean_raw_predictions.argmax(dim=-1)
    return mean_predictions, None, mean_raw_predictions, std_raw_preditions
   
def eval_datamodule(args, model, datamodule):

    print("\nStart infering ...")

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    # Initialize data loaders
    trainloader_test = datamodule.get_train_dataloader(shuffle=False) # used for calculating statistics after every epoch (similar to vali/test)
    valiloader = datamodule.get_val_dataloader()
    testloader = datamodule.get_test_dataloader()
    dl_dict = {
        "Train": trainloader_test,
        "Val": valiloader,
        "Test": testloader
    }

    results = {}    
    result_metrics = {
        "targets": list(args.Data.targets_to_train)
    }
    for dl_name, dl in dl_dict.items():
        # note that mean_for_norm and std_for_norm is None 
        if not (dl is None):
            _, dl_result_metric, dl_mol_ids, dl_smiles, dl_extra_inputs, dl_raw_real_values, dl_raw_predictions, dl_real_values, dl_predictions = eval(
                model=model,
                loader=dl,
                args=args,
                device=device,
                mean_for_norm=datamodule.mean_for_norm,
                std_for_norm=datamodule.std_for_norm,
                keep_infer_results=True
                )
            dl_results = {
                'metrics': dl_result_metric,
                'mol_ids': dl_mol_ids, 
                'SMILES': dl_smiles,
                'extra_input': dl_extra_inputs,
                'raw_real_values': dl_raw_real_values,
                'raw_predictions': dl_raw_predictions,
                'real_values': dl_real_values,
                'predictions': dl_predictions
            }

            results[dl_name] = dl_results
            result_metrics[f"{dl_name} metric"] = dl_result_metric
            
        if args.verbose:
            print(f"\n---> Final metrics: {result_metrics}")

    print("\n... infering finished.")
            
    return results

