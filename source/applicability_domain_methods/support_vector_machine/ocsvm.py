##
#  @file example_training.py
#
#  @brief Training of suppot vector regression model and export to file that is readable by MeLOn.
#
# ==============================================================================\n
#   Aachener Verfahrenstechnik-Systemverfahrenstechnik, RWTH Aachen University  \n
# ==============================================================================\n
#
#  @author Linus Netze, Artur M. Schweidtmann and Alexander Mitsos
#  @date 28. March 2020
#  @modified by Jan G. Rittig, Artur M. Schweidtmann, and Alexander Mitsos
#  @date 2. December 2020
#  @modified by Moritz Wedemeyer,  Jan G. Rittig, and Alexander Mitsos
#  @date 2. January 2022
##

from abc import ABC, abstractmethod

#import utils
from sklearn.svm import OneClassSVM
import numpy as np
import time
import os.path as osp
import pandas as pd
import torch
import pickle


class SVM_AD(ABC):
    """Class for Applicability domain (AD) via support vector machine (SVM)
    Parameters
    ---------
    dataset: string
        Name of the dataset model should be/is trained on.
    model_name: string, optional
        Name of the model that is used for saving model parameters. Should be unique.
        By default: "gnn_dataset"
    kernel: string, optional
        Kernel that will be used for SVM (corresponding to sklearn kernels)
        By default: "rbf"
    gamma: string, optional
        Hyperparameter gamma that will be used for SVM (corresponding to sklearn gamma)
        By default: "scale" (autoscale)
    nu: float, optional
        Hyperparameter nu that will be used for SVM (corresponding to sklearn kernels)
        By default: 0.01
    tol: string, optional
        tolerance that will be used for SVM
        By default: 1e-10
    """

    # init model
    def __init__(self, model_name='svm_ad', dataset=None, kernel='rbf', gamma='scale', nu=0.01, tol=1e-10, **kwargs):
        print("Start applicability domain with one-class SVM ...")
        self._model_name = str(model_name)
        self._dataset = dataset
        self._set_hyperparameters(kernel, gamma, nu, tol, first_init=True)
        self._svm = self._internal_sklearn_init()
    
    # initialize sklearn's OneClassSVM with selected hyperparameters
    def _internal_sklearn_init(self):
        return OneClassSVM(kernel=self._kernel, gamma=self._gamma, nu = self._nu, tol = self._tol)

    # setter for hyperparameters
    def _set_hyperparameters(self, kernel=None, gamma=None, nu=None, tol=None, first_init=False):
        if kernel is not None: self._kernel = kernel
        if gamma is not None: self._gamma = gamma
        if nu is not None: self._nu = nu
        if tol is not None: self._tol = tol
        if not first_init:
            print(f"Warning: you changed the hyperparameter settings, the current svm model {self.model_name()} will be replaced")
            self._svm = self._internal_sklearn_init()

    # getter for hyperparameters
    def _get_hyperparameters(self):
        return self._kernel, self._gamma, self._nu, self._tol

    @property
    def model_name(self):
        """Model name"""
        return self._model_name

    @property
    def dataset(self):
        """Dataset the model is trained on."""
        return self._dataset

    @property
    def hyperparameters(self):
        """Dataset the model is trained on."""
        return self._get_hyperparameters()

    @property
    def svm(self):
        """SVM that is trained"""
        return self._svm


    # train model on given dataset <train_data> that should be a 2-dimensional array with latent vectors of training input graphs (#graphs x latent vector dim)
    def train(self, train_data=None, args=None, model=None, datamodule=None):
        
        if args.verbose:
            print("\n---> Process data for SVM training.")
        # check if dataset is provided
        if train_data is None and self._dataset is None:
            if model is not None and datamodule is not None and args is not None:
                print(f"\n---> Training data for one-class SVM is extracted directly from the model (latent vectors of training and validation data).")
                train_latent_data = self._from_model(model=model, dataloader=datamodule.get_train_dataloader(batch_size=args.Training.val_batch_size, shuffle=False))
                vali_latent_data = self._from_model(model=model, dataloader=datamodule.get_val_dataloader(batch_size=args.Training.val_batch_size))
                train_data = np.concatenate([train_latent_data, vali_latent_data])
                self._dataset = train_data
            else:
                print(f"\n---> No dataset for training found. Please provide a dataset")
        # if no explicit training dataset is defined, check if dataset was defined in initialization
        elif self._dataset is not None:
            train_data = self._dataset
        else: 
            print(f"\n---> No dataset for training found. Please provide a dataset")

        # output filename
        #filename_out = os.path.join(train_data, "Output")

        if args.verbose:
            print("\n---> Start training of SVM.")
        training_time = time.time()

        self._svm.fit(train_data)

        save_dir = f"{osp.dirname(osp.abspath(__file__))}/../../../{args.save_dir}/"
        self.save_model(save_dir=save_dir)

        training_time = time.time() - training_time

        if args.verbose:
            print(f"\n---> Training of {self._model_name} finished after {training_time:.6f} seconds.")

    def infer(self, infer_data=None, args=None, model=None, datamodule=None):
        if args.verbose:
            print("\n---> Process data for SVM infering.")
        # check if dataset is provided
        if infer_data is None:
            if model is not None and datamodule is not None and args is not None:
                train_loader = datamodule.get_train_dataloader(batch_size=args.Training.val_batch_size, shuffle=False)
                train_latent_data, train_mol_id = self._from_model(model=model, dataloader=train_loader, get_mol_id=True)
                infer_data = {
                    'Train': {
                        'latent_vectors': train_latent_data, 
                        'mol_ids': train_mol_id,
                    }
                }
                val_loader = datamodule.get_val_dataloader(batch_size=args.Training.val_batch_size)
                if not (val_loader is None):
                    vali_latent_data, vali_mol_id = self._from_model(model=model, dataloader=val_loader, get_mol_id=True)
                    infer_data['Val'] = { 
                        'latent_vectors': vali_latent_data, 
                        'mol_ids': vali_mol_id,
                    }
                test_loader = datamodule.get_test_dataloader(batch_size=args.Training.val_batch_size)
                if not (test_loader is None):
                    test_latent_data, test_mol_id = self._from_model(model=model, dataloader=test_loader, get_mol_id=True)                    
                    infer_data['Test'] = { 
                        'latent_vectors': test_latent_data, 
                        'mol_ids': test_mol_id,
                    }
            else:
                print(f"\n---> No dataset for infering found. Please provide a dataset")
        # if no explicit training dataset is defined, check if dataset was defined in initialization
        elif self._dataset is not None:
            infer_data = [self._dataset]
            print("\n---> No dataset for infering found. Using dataset stored for initialization/training.")
        else: 
            print(f"\n---> No dataset for infering found. Please provide a dataset")
        
        if args.verbose:
            print("\n---> Infering of SVM.")

        if isinstance(infer_data, dict):
            for data_set_name in infer_data.keys():
                infer_data_i = infer_data[data_set_name]['latent_vectors']
                infer_data_i_results = self.predict(infer_data_i)
                infer_data[data_set_name]["validity"] = infer_data_i_results
                infer_data[data_set_name]["metrics"] = {
                    "valid": np.sum(infer_data_i_results == 1),
                    "invalid": np.sum(infer_data_i_results == -1)
                }
            return infer_data
        elif isinstance(infer_data, np.array):
            return self.predict(infer_data)


    def _from_model(self, model, dataloader, get_mol_id=False):
        # svm is trained on training and validation data # TODO: decide if validation data should be inclduded
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        latent_vectors = torch.tensor([]).to(device)
        mol_id = []
        for data in dataloader:
            model.eval()
            data = data.to(device)
            latent_vectors = torch.cat((latent_vectors, model(data, latent_mode=True)))
            if get_mol_id:
                mol_id.append(data.idx)
        latent_vectors = np.array(latent_vectors.detach().to('cpu'), dtype=np.float)
        if get_mol_id:
            return latent_vectors, mol_id
        return latent_vectors


    # predict whether data points within infer_data lie within the applicability domain (AD)
    def predict(self, infer_data):
        return self._svm.predict(infer_data)


    # save model to dictionary for checkpointing with pytorch
    def save_model(self, save_dir, model_name=None):
        if model_name:
            print(f"Model is renamed to {model_name} for saving.")
            self._model_name = model_name
        model_save_dict = {'model': self._svm, 'dataset': self._dataset, 'hyperparameters': self._get_hyperparameters()}
        save_path = osp.join(save_dir, self._model_name + '.pkl')
        pickle.dump(model_save_dict, open(save_path, 'wb'))
        print(f"\n---> SVM AD model is save to {save_path}")

        # Save model to JSON
        #utils.save_model_to_json(filename_out, dataset_name + '.json', clf)

    # load model from given dict
    def load_model(self, load_dir, model_load_dict, model_name="svm_ad"):
        model_load_dict = pickle.load(open(osp.join(load_dir, self._model_name + '.pkl', 'wb')))
        self._model_name = model_name
        hyperp = model_load_dict['hyperparameters']
        self._set_hyperparameters(kernel=hyperp[0], gamma=hyperp[1], nu=hyperp[2], tol=hyperp[3], first_init=True)
        self._svm = model_load_dict['model']
        self._dataset = model_load_dict['dataset']


    def read_csv_data(self, file_path, model_id=None, additional_columns=[]):
        """ Read latent space data stored in csv file (please note csv file should have column named 'latent_vector{_<model_id>}' with latent space arrays for each graph input.
        Arguments
        ---------
        file_path: string
            Filepath to csv file wherein the input data is stored (either for train or test data).
        model_id: int, optional
            Identifier for GNN model. Only necessary when GNN ensemble is used, otherwised redundant. 
            By default: None
        additional_columns: array-like, optional
            If additional columns should be read in, the column names can be defined here. An additional array is returned.
            By default: []
        Returns
       -------
        latent_vectors (, additional_columns)
           Returns latent vectors in array format (+ additional columns in array format)
        """

        # only use model identifier for ensemble models
        if model_id != None:
            model_id = "_" + str(model_id)
        else:
            model_id = ""

        # read data from csv file
        df_data = pd.read_csv(file_path, sep=',', engine= "python")
    
        # only select latent vectors and transform those to numpy arrays
        latent_vectors = df_data["latent_vector{}".format(model_id)].dropna().values
        latent_vectors = np.array([np.fromstring(x[1:-1], dtype=np.float, sep=',') for x in latent_vectors])
        #pred_train = df_train[["predicted_DCN_GNN{}".format(model_id),"predicted_MON_GNN{}".format(model_id),"predicted_RON_GNN{}".format(model_id)]].dropna().values
        #meas_train = df_train[["measured DCNs", "measured MONs", "measured RONs"]].values
        #train_error = pred_train - meas_train

        if len(additional_columns) == 0:
            return latent_vectors
        else:
            add_columns = df_data[additional_columns].values
            return latent_vectors, add_columns

'''
def test_example():

    ############################ LOAD DATA ############################# 
    dataset_name = "GNN_FIQ_Schweidtmann_2020"
    data_path = osp.join("../../data", "latent_space", dataset_name)
    training_data_path = osp.join(data_path, "Input", "latent_train.csv")
    test_data_path = osp.join(data_path, "Input", "latent_test.csv")

    ############################ INIT MODEL ############################ 
    test_svm = SVM_AD(model_name="tester")

    ########################### TRAIN MODEL ############################ 
    train_latents = test_svm.read_csv_data(training_data_path, model_id=1)
    test_svm.train(train_latents)

    ########################## INFER W/ MODEL ########################## 
    test_latents, add_columns = test_svm.read_csv_data(test_data_path, model_id=1, additional_columns=['measured DCNs', 'predicted_DCN_GNN_1'])
    print(test_svm.predict(test_latents))
    #print(add_columns)

    ##################### TEST MODEL SAVING/LOADING #################### 
    test_svm.save_model("Test")
    test_svm_2 = SVM_AD(model_name='tester_2')
    test_svm_2.load_model("Test", "tester")
    assert all(test_svm.predict(test_latents) == test_svm_2.predict(test_latents))
    assert test_svm.hyperparameters == test_svm_2.hyperparameters
'''
