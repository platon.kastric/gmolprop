import os
import wandb
from tqdm import tqdm

import torch
import torch.nn as nn

from utils.early_stopping import EarlyStopping

from . import inference
   

def train_model(args, model, datamodule):
    
    print("Start training of property prediction model...")

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model.to(device)

    if args.Logging.log_wandb:
        wandb.init(
            config=dict(args.Model), 
            entity=args.Logging.wandb_entity, 
            project=args.Logging.wandb_project, 
            name=args.Logging.experiment_name
            )

    save_path = f"{os.path.dirname(os.path.abspath(__file__))}/../{args.save_dir}/model.pt"
    os.makedirs(os.path.dirname(save_path), exist_ok=True)
    if args.Logging.log_wandb:
        wandb.config.update({"save_path": save_path}, allow_val_change=True)

    # Initialize data loaders
    trainloader = datamodule.get_train_dataloader(batch_size=args.Training.train_batch_size, seed=args.random_seed, shuffle=True)
    trainloader_test = datamodule.get_train_dataloader(batch_size=args.Training.val_batch_size, shuffle=False) # used for calculating statistics after every epoch (similar to vali/test)
    valiloader = datamodule.get_val_dataloader(batch_size=args.Training.val_batch_size)
    testloader = datamodule.get_test_dataloader(batch_size=args.Training.val_batch_size)

    # Define the loss function and optimizer
    def loss_function(y_true, y_pred):
        # Filter data (if data point is not available in real data, it should have "inf" as value)
        y_pred = y_pred[torch.isfinite(y_true)]
        y_true = y_true[torch.isfinite(y_true)]
        # Select loss function according to task
        if args.Model.task_type == "regression":
            loss_f = nn.MSELoss()
        elif args.Model.task_type == "classification":
            if args.Model.num_classes == 2:
                loss_f = nn.BCELoss()
            else:
                loss_f = nn.CrossEntropyLoss()
        return loss_f(y_pred, y_true)
    if args.Training.optim == "sgd":
        optimizer = torch.optim.SGD(model.parameters(), lr=args.Training.lr, momentum=args.Training.momentum)
    elif args.Training.optim == "adam":
        optimizer = torch.optim.Adam(model.parameters(), lr=args.Training.lr)

    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
        optimizer, factor=args.Training.lr_factor, patience=args.Training.lr_patience, min_lr=args.Training.lr_min)

    early_stopping = EarlyStopping(patience=args.Training.early_stopping_patience, verbose=True, save_path=save_path)

    # Training
    def train_epoch(loader):
        model.train()
        loss_all = 0

        for data in tqdm(loader):
            data = data.to(device)
            optimizer.zero_grad()
            tmp_pred_data = model(data).view(-1)
            tmp_real_data = data.y.view(-1)
            loss = loss_function(y_true=tmp_real_data, y_pred=tmp_pred_data)
            loss.backward()
            optimizer.step()
            loss_all += loss.item() * data.num_graphs

        return loss_all / len(loader.dataset)
    
    
    print(f"\n---> Training with max number of epochs: {args.Training.epochs} ...")
    best_epoch_avg_validation_score = None
    scores_epoch = [args.Training.metric] + args.Training.extra_metrics
    for epoch in range(1, args.Training.epochs+1):
        print(f"\n---> Run epoch {epoch}:")
        lr = scheduler.optimizer.param_groups[0]['lr']
        loss = train_epoch(trainloader)
        raw_vali_metric_epoch, vali_metric_epoch = inference.eval(
            model=model,
            loader=valiloader, 
            args=args,
            device=device,
            scores=scores_epoch,
            mean_for_norm=datamodule.mean_for_norm,
            std_for_norm=datamodule.std_for_norm,
            )
        if not (testloader is None):
            raw_test_metric_epoch, test_metric_epoch = inference.eval(
                model=model,
                loader=testloader, 
                args=args,
                device=device,
                scores=scores_epoch,
                mean_for_norm=datamodule.mean_for_norm,
                std_for_norm=datamodule.std_for_norm,
                )
        else: 
            raw_test_metric_epoch, test_metric_epoch = None, None
        raw_train_metric_epoch, train_metric_epoch = inference.eval(
            model=model,
            loader=trainloader_test, 
            args=args,
            device=device,
            scores=scores_epoch,
            mean_for_norm=datamodule.mean_for_norm,
            std_for_norm=datamodule.std_for_norm,
            )

        if raw_train_metric_epoch is not None:
            print(f"\n\tEpoch: {epoch}, LR: {lr:.4f}, Loss: {loss:.5f}")
            print(f"\t\tTrain norm {args.Training.metric}: {raw_train_metric_epoch[args.Training.metric]}, Train {args.Training.metric}: {train_metric_epoch[args.Training.metric]}")
            print(f"\t\tVali norm {args.Training.metric}: {raw_vali_metric_epoch[args.Training.metric]}, Vali {args.Training.metric}: {vali_metric_epoch[args.Training.metric]}")
            if not (test_metric_epoch is None):
                print(f"t\tTest norm {args.Training.metric}: {raw_test_metric_epoch[args.Training.metric]}, Test {args.Training.metric}: {test_metric_epoch[args.Training.metric]}")
        else:
            print(f"\n\tEpoch: {epoch}, LR: {lr:.4f}, Loss: {loss:.5f}")
            print(f"\t\tTrain {args.Training.metric}: {train_metric_epoch[args.Training.metric]}")
            print(f"\t\tVali {args.Training.metric}: {vali_metric_epoch[args.Training.metric]}")
            if not (test_metric_epoch is None):
                print(f"\t\tTest {args.Training.metric}: {test_metric_epoch[args.Training.metric]}")


        # Get overall scores for lr-scheduler and early stopping
        epoch_avg_validation_score = raw_vali_metric_epoch[args.Training.metric].sum() # note that sum is generalization for multi-task model, for single-task model only 1 entry

        scheduler.step(epoch_avg_validation_score)

        if (best_epoch_avg_validation_score is None) or epoch_avg_validation_score < best_epoch_avg_validation_score:
            best_epoch = epoch
            best_epoch_avg_validation_score = epoch_avg_validation_score
            best_epoch_avg_train_score = raw_train_metric_epoch[args.Training.metric].sum()
            best_epoch_avg_test_score = raw_test_metric_epoch[args.Training.metric].sum() if raw_test_metric_epoch is not None else None
            best_epoch_validation_score, best_epoch_test_score, best_epoch_train_score = vali_metric_epoch, test_metric_epoch, train_metric_epoch

        early_stopping(epoch_avg_validation_score, model)

        # log to wandb
        if args.Logging.log_wandb:
            cur_state = {
                'epoch': epoch, 
                'train_loss': loss
                }
            if args.Data.num_targets > 1:
                best_epoch_avg_state ={
                    'best_epoch': best_epoch, 
                    f'best_epoch_avg_train_{args.Training.metric}_normalized': best_epoch_avg_train_score, 
                    f'best_epoch_avg_validation_{args.Training.metric}_normalized': best_epoch_avg_validation_score,
                    f'best_epoch_avg_test_{args.Training.metric}_normalized': best_epoch_avg_test_score
                    }
            else:
                best_epoch_avg_state = {}
            best_epoch_single_target_state = {
                **{f"best_epoch_train_{k}_{t}": v[t_i] for t_i, t in enumerate(args.Data.targets_to_train) for k, v in best_epoch_train_score.items()},
                **{f"best_epoch_validation_{k}_{t}": v[t_i] for t_i, t in enumerate(args.Data.targets_to_train) for k, v in best_epoch_validation_score.items()},
                **{f"best_epoch_test_{k}_{t}": v[t_i] for t_i, t in enumerate(args.Data.targets_to_train) for k, v in best_epoch_test_score.items() if best_epoch_test_score is not None},
            }
            log_states = {
                **cur_state, 
                #**{f"norm_train_{k}_{t}": v[t_i] for t_i, t in enumerate(args.Data.targets_to_train) for k, v in raw_train_metric_epoch.items()},
                **{f"train_{k}_{t}": v[t_i] for t_i, t in enumerate(args.Data.targets_to_train) for k, v in train_metric_epoch.items()}, 
                #**{f"norm_vali_{k}_{t}": v[t_i] for t_i, t in enumerate(args.Data.targets_to_train) for k, v in raw_vali_metric_epoch.items()}, 
                **{f"vali_{k}_{t}": v[t_i] for t_i, t in enumerate(args.Data.targets_to_train) for k, v in vali_metric_epoch.items()}, 
                #**{f"norm_test_{k}_{t}": v[t_i] for t_i, t in enumerate(args.Data.targets_to_train) for k, v in raw_test_metric_epoch.items()}, 
                **{f"test_{k}_{t}": v[t_i] for t_i, t in enumerate(args.Data.targets_to_train) for k, v in test_metric_epoch.items() if test_metric_epoch is not None},
                **best_epoch_avg_state,
                **best_epoch_single_target_state,              
                }
            wandb.log(log_states, step=epoch)
            print("Logged")

        # if early stopping or max number epochs is reached
        if early_stopping.early_stop or epoch == args.Training.epochs:
            if early_stopping.early_stop:
                print(f'\n---> Validation error has not decreased in {args.Training.early_stopping_patience} epochs, thus training is stopped early at epoch {epoch}.')
            else:
                print(f'\n---> Training ended at maximum epoch ({epoch})')
            print(f'\n... finished training of property prediction model. Model saved at: {save_path}')
            model.load_state_dict(torch.load(save_path))
            return model

