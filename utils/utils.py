import random
import os
import numpy as np
import torch
import json
import pandas as pd
from omegaconf import OmegaConf

def seed_everything(seed: int):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = True

def check_model_path(model_path):
    # Check model path
    checked_model_path = None
    if os.path.isfile(model_path):
        checked_model_path = model_path
    elif os.path.isdir(model_path):
        if os.path.isfile(os.path.join(model_path, "model.pt")):
            checked_model_path = os.path.join(model_path, "model.pt")
        else:
            raise FileNotFoundError(f"No model file found at {model_path}. Please provide a valid path.")
    else:
        raise FileNotFoundError(f"No model file found at {model_path}. Please provide a valid path.")
    return checked_model_path

def load_config(config_path, model_path=None):
    ## Path
    if config_path is None:
        if model_path is None:
            raise ValueError("No config path provided.")
        model_dir = os.path.dirname(model_path)
        config_path = os.path.join(model_dir, "config.yaml")
        if not os.path.isfile(config_path):
            raise FileNotFoundError(f"No config file found at {config_path}. Please provide a valid path.")
    else:
        if os.path.isdir(config_path):
            if os.path.isfile(os.path.join(config_path, "config.yaml")):
                config_path = os.path.join(config_path, "config.yaml")
            else:
                raise FileNotFoundError(f"No config file found at {config_path}. Please provide a valid path.")
        elif not os.path.isfile(config_path):
            raise FileNotFoundError(f"No config file found at {config_path}. Please provide a valid path.")
    with open(config_path, "r") as c_f:
        cfg = OmegaConf.load(c_f)
    return cfg

def json_convert_tensor_tolist(x):
    if hasattr(x, "tolist"):
        return x.tolist()
    raise TypeError(x)

def save_results_to_csv(args, save_dir_results, results, sort=True, real_values=True):
    # Model results
    for tmp_results_name, tmp_results in results["Prediction_results"].items():
        tmp_data_dir = {
            "mol_id": tmp_results["mol_ids"],
        }
        for s_i, s_name in enumerate(args.Data.smiles_columns):
            tmp_data_dir[s_name] = tmp_results["SMILES"][s_i]
        if not (args.Data.extra_input_columns is None):
            extra_column_counter = 0
            for extra_columns_per_target in args.Data.extra_input_columns:
                for extra_column in extra_columns_per_target:
                    if extra_column not in ["None", None]:
                        tmp_data_dir[extra_column] = tmp_results["extra_input"][:,extra_column_counter]
                        extra_column_counter += 1
        for t_i, t_name in enumerate(args.Data.targets_to_train):
            if real_values:
                tmp_data_dir[f"Real_{t_name}"] = tmp_results["real_values"][:,t_i]
            tmp_data_dir[f"Pred_{t_name}"] = tmp_results["predictions"][:,t_i]
            if args.task_type == "classification": # save raw predictions by GNN (used for classification as predictions provides class probabilities)
                tmp_data_dir[f"Probability_{t_name}"] = tmp_results["raw_predictions"][:,t_i]
            if "stds" in tmp_results.keys(): # save standard deviation if provided in results, e.g., for ensemble
                tmp_data_dir[f"Pred_{t_name}_std"] = tmp_results["stds"][:,t_i]

        # AD results
        if "AD_results" in results.keys():
            if tmp_results_name in results["AD_results"].keys():
                tmp_ad_results = results["AD_results"][tmp_results_name]
                tmp_data_dir["Validity"] = tmp_ad_results["validity"] 

        df_tmp = pd.DataFrame(tmp_data_dir)
        if sort:
            df_tmp = df_tmp.reindex(index=df_tmp["mol_id"].str.rsplit("_").str[-1].astype(int).sort_values().index)         
        df_tmp.to_csv(os.path.join(save_dir_results, f"{tmp_results_name}.csv"), index=False)

def save_results_metrics(args, save_dir_results, results):
    result_metrics = {
        "targets": list(args.Data.targets_to_train),
    }
    for tmp_results_name, tmp_results in results["Prediction_results"].items():
        write_metrics_to_txt(metric_dicts=tmp_results["metrics"], args=args,path=os.path.join(save_dir_results, f"{tmp_results_name}_metrics.txt"))
        result_metrics[f"{tmp_results_name} metric"] = tmp_results["metrics"]
        if "AD_results" in results.keys():
            result_metrics[f"{tmp_results_name} AD metric"] = results["AD_results"][tmp_results_name]["metrics"]
    with open(os.path.join(save_dir_results, "result_metrics.json"), "w") as rm:
        json.dump(result_metrics, rm, default=json_convert_tensor_tolist)   

def save_results(args, results, save_dir_results=None):
    if save_dir_results is None:
        save_dir_results = f"{os.path.dirname(os.path.abspath(__file__))}/../{args.Result_analysis.save_dir_results}"
    os.makedirs(os.path.dirname(save_dir_results), exist_ok=True)
    # TODO: save config
    if args.Result_analysis.pred_to_csv:
        save_results_to_csv(args, save_dir_results, results)
    if args.Result_analysis.save_result_metric:
        save_results_metrics(args, save_dir_results, results)

def write_metrics_to_txt(metric_dicts, args, path):
    with open(path, "w") as fp:
        for metric_name, metric in metric_dicts.items():
            if metric_name in ['confusion_matrix']: continue # catch metrics which cannot be expressed in one value # TODO: need to change this?
            fp.write(f"\n\n### {metric_name} ###")
            for t_i, t in enumerate(args.Data.targets_to_train):
                if isinstance(metric, dict):
                    fp.write("{:<20s}".format(f"\n\t{t}:"))
                    for k, v in metric.items():
                        fp.write("{:<20s}{:<20s}".format(f"\n\t\t{k}:", f"{v.unsqueeze(-1)[t_i].item():.7f}"))
                else:
                    fp.write("{:<20s}".format(f"\n\t{t}:"))
                    fp.write("{:<20s}".format(f"{metric.unsqueeze(-1)[t_i].item():.7f}"))

def read_data_to_df(path, data_args, without_target=False):
    # get dataframe of SMILES and target values
    cols = []
    cols += data_get_input_cols(data_args=data_args)
    output_cols = data_get_output_cols(data_args=data_args) 
    if (not (output_cols is None)) and (not without_target):
        cols += output_cols 
    df = pd.read_csv(path, usecols=cols, sep=data_args.csv_separator, engine="python")
    return df

def get_all_smiles_from_df(df, smiles_columns):
    smiles_list = [df[smiles_column].to_list() for smiles_column in smiles_columns]
    smiles_list = [s for sub_smiles_list in smiles_list for s in sub_smiles_list]
    return smiles_list

def data_get_input_cols(data_args):
    cols = []
    cols += data_args.smiles_columns
    if not (data_args.extra_input_columns is None):
        cols += [add_col for add_cols_prop_i in data_args.extra_input_columns for add_col in add_cols_prop_i if not (add_col is None)]
    return list(cols)

def data_get_output_cols(data_args):
    if not (data_args.target_columns is None):
        return list(data_args.target_columns)
    else:
        return None
