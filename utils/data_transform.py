from typing import List
import torch
import pandas as pd


def standardize(y, mean, std):
    return (y - mean) / std


def reverse_standardize(y, mean, std):
    return y * std + mean


def log_transform(y):
    return torch.log(y)


def reverse_log_transform(y):
    return torch.exp(y)


def split_data(dataset, train_fraction=0.8, vali_fraction=0.1, test_fraction=0.1):

    if (train_fraction + vali_fraction + test_fraction) != 1:
        raise ValueError("Dataset train/vali/test fractions do not add up to 1.")

    # dataset info
    data_len = len(dataset)
    idxs = list(range(data_len))

    # test split
    test_len = data_len * test_fraction
    test_idxs = idxs[: int(test_len)]
    not_test_idxs = idxs[int(test_len) :]

    # train, vali split
    vali_len = data_len * vali_fraction
    vali_idxs = not_test_idxs[: int(vali_len)]
    train_idxs = not_test_idxs[int(vali_len) :]

    # sanity check idxs
    for i in test_idxs:
        if i in train_idxs or i in vali_idxs:
            raise ValueError("Test data in training or validation set.")
    for i in vali_idxs:
        if i in train_idxs or i in test_idxs:
            raise ValueError("Vali data in training or test set.")
    for i in train_idxs:
        if i in test_idxs or i in vali_idxs:
            raise ValueError("Training data in test or validation set.")

    # splitting data according to indices
    train_set = dataset[train_idxs].copy()
    if vali_fraction > 0:
        vali_set = dataset[vali_idxs].copy()
    else: 
        vali_set = None
    if test_fraction > 0:
        test_set = dataset[test_idxs].copy()
    else: 
        test_set = None

    # sanity check length
    if vali_fraction > 0 and test_fraction > 0:
        if len(train_set) + len(vali_set) + len(test_set) != len(dataset):
            raise ValueError(
                f"Number of data points in training {len(train_set)} + validation {len(vali_set)} + test set {len(test_set)} does not equal raw dataset. Something went wrong during splitting the data."
            )
    elif vali_fraction > 0:
        if len(train_set) + len(vali_set) != len(dataset):
            raise ValueError(
                f"Number of data points in training {len(train_set)} + validation {len(vali_set)} does not equal raw dataset. Something went wrong during splitting the data."
            )
    elif test_fraction > 0:
        if len(train_set) + len(test_set) != len(dataset):
            raise ValueError(
                f"Number of data points in training {len(train_set)} + test {len(test_set)} does not equal raw dataset. Something went wrong during splitting the data."
            )

    return train_set, vali_set, test_set


def remove_outliers(df: pd.DataFrame, columns: List[str], std_devs: int = 1):
    """Remove outliers from a dataframe based on the number of standard deviations.

    Parameters
    ----------
    df : pd.DataFrame
        Dataframe to remove outliers from.
    columns : List[str]
        List of columns to remove outliers from.
    std_devs : int, optional
        Number of standard deviations to use as a threshold, by default 1


    Returns
    -------
    pd.DataFrame
        Dataframe with outliers removed.

    """
    for col in columns:
        mean = df[col].mean()
        std = df[col].std()
        tmp_lower_bound = mean - std_devs * std
        tmp_upper_bound = mean + std_devs * std
        df = df[(df[col] >= tmp_lower_bound) & (df[col] <= tmp_upper_bound)]

    return df
