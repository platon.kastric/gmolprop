"""Contains custom OmegaConf resolvers, import this in your main script so hydra can load the config properly"""
from omegaconf import OmegaConf

OmegaConf.register_new_resolver("len", lambda lst: len(lst))
OmegaConf.register_new_resolver("default_metric", lambda x: "mse" if x == "regression" else "log_loss")
OmegaConf.register_new_resolver("default_extra_metrics", lambda x: ["r2", "mae"] if x == "regression" else ["balanced_accuracy", "sensitivity", "specificity"])