Ad Methods
==========

ADCallback
----------

.. automodule:: ad_methods.ADCallback
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: ad_methods
   :members:
   :undoc-members:
   :show-inheritance:
