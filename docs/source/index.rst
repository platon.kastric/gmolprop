Welcome to GNN4PP's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Examples_and_Introduction
   modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
