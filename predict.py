import os 
import numpy as np
import torch
import argparse
from datetime import datetime
import shutil
import pandas as pd

# hydra
import hydra
import utils.omegaconf_resolvers
from omegaconf import OmegaConf

from torch_geometric.loader import DataLoader

import utils.utils as utils
from utils.Mol_features import MolFeatures
from source import inference
from source.models.graph_neural_network import GNN
from source.pyg_molgraph import PyGMolgraphDataset

model_catalog = {
    "GNN": GNN
} 

def prepare_prediction_data(args, model_cfg):

    # argparse provides:
    # 1. model path/dir
    # 2. molecules (csv-path or simple list)
    # optional: config path (default: in model dir)

    # Check data path
    data_path, smiles = None, None
    if not (args.predict_data_path is None):
        if os.path.isfile(args.predict_data_path):
            data_path = args.predict_data_path
            data_dir = os.path.dirname(data_path)
        else:
            FileNotFoundError(f"No data file found at {args.predict_data_path}. Please provide a valid path.")
    elif not (args.SMILES is None):
        smiles = args.SMILES
    else:
        raise ValueError("No input molecules retrieved. Please provide molecules via args --predict_data_path or --SMILES.")

    # Mols to predict
    ## TODO: should we allow for extra featurization (not included in training -> model will not be sensitive to this)
    ## Make temporary CSV file with data to be predicted
    tmp_dir = os.path.join("./data/05_model_output", f"tmp_data_{datetime.now().strftime('%Y-%m-%d_%H:%M:%S')}")
    os.makedirs(tmp_dir)
    os.makedirs(os.path.join(tmp_dir, "raw"))
    try:
        ## 1. Input as CSV file with mols and potential extra inputs
        if not (data_path is None):
            shutil.copy(data_path, os.path.join(tmp_dir, "raw", "raw.csv"))
        ## 2. Input via command line
        else:
            data_dict = {}
            for smiles_idx, smiles_col in enumerate(model_cfg.Data.smiles_columns):
                data_dict[smiles_col] = smiles.split(';')[smiles_idx]
            if not(model_cfg.Data.extra_input_columns is None):
                if any(c != ['None'] for c in model_cfg.Data.extra_input_columns):
                    if args.extra_input is None:
                        raise ValueError(f"Model requires additional input information {model_cfg.Data.extra_input_columns}. Please provide via args --extra_input.") 
                    extra_column_counter = 0
                    for extra_columns_per_target in model_cfg.Data.extra_input_columns:
                        for extra_column in extra_columns_per_target:
                            if extra_column not in ["None", None]:
                                data_dict[extra_column] = args.extra_input.split(";")[extra_column_counter]
                                extra_column_counter += 1
            df = pd.DataFrame([data_dict])
            df.to_csv(os.path.join(tmp_dir, "raw", "raw.csv"), sep=model_cfg.Data.csv_separator)
        ## Extract mol featurization from cfg
        mol_features = {}
        atom_features = MolFeatures()
        atom_features.from_yaml(feat_type="atom", mol_feature_mapping_yaml=model_cfg.Model.input_featurization_atom)
        mol_features["atom"] = atom_features
        bond_features = MolFeatures()
        bond_features.from_yaml(feat_type="bond", mol_feature_mapping_yaml=model_cfg.Model.input_featurization_bond)
        mol_features["bond"] = bond_features
        ## Create Molgraph data set
        dataset = PyGMolgraphDataset(
            root=tmp_dir,
            args=model_cfg.Data,
            mol_features=mol_features,
            without_target=True,
        )    
        ## Remove temporary dir for data consutrction
        try:
            shutil.rmtree(tmp_dir)
        except OSError as e:
            print("Error: %s - %s." % (e.filename, e.strerror))
    except:
        shutil.rmtree(tmp_dir)
        print("Something went wrong in the data processing.")
        raise SyntaxError
    ## Get data loader
    follow_batch = [f"x_{i}" for i in range(len(model_cfg.Data.smiles_columns))]
    dataloader = DataLoader(dataset, batch_size=model_cfg.Training.val_batch_size, follow_batch=follow_batch)
    return dataloader, dataset

def predict(args):

    model_path_list, cfg_path_list = [], []
    if args.ensemble_path is None:
        if args.model_path is None:
            raise ValueError(f"No model path provided. Please use --model_path.")
        else:
            model_path_list.append(args.model_path)
            cfg_path_list.append(args.config_path)
    else:
        ensemble_cfg = utils.load_config(config_path=args.ensemble_path)
        model_path_list = ensemble_cfg.Ensemble.model_source_list
        cfg_path_list = [None for _ in range(len(model_path_list))] # not possible to provide custom cfg path for enesemble (should be in same dir)

    previous_cfg = None
    dataloader_list = []
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model_list = []
    for model_idx, model_path in enumerate(model_path_list):
        # Check model path
        model_path = utils.check_model_path(model_path)
        # Get config
        config_path = cfg_path_list[model_idx]
        cfg = utils.load_config(config_path=config_path, model_path=model_path)
        # Setup model
        model = model_catalog[cfg.Model.type](**dict(cfg.Model))
        model.load_state_dict(torch.load(model_path))
        model.to(device)
        model_list.append(model)
        # Setup data
        ## Init dataloader 
        if (previous_cfg is None):
            dataloader, dataset = prepare_prediction_data(args=args, model_cfg=cfg)
            dataloader_list.append(dataloader)
        ## If featurization changes, dataloader differs -> reload with new features
        elif (previous_cfg.Model.input_featurization_atom != cfg.Model.input_featurization_atom) or (previous_cfg.Model.input_featurization_bond != cfg.Model.input_featurization_bond):
            dataloader, tmp_dataset = prepare_prediction_data(args=args, model_cfg=cfg)
            dataloader_list.append(dataloader)
            # Check if inputs are the same
            if any([dataset.data[f"smiles_mol_{i}"] != tmp_dataset.data[f"smiles_mol_{i}"] for i in range(len(cfg.Data.smiles_columns))]) or any([dataset.data.graph_features.unsqueeze(-1) != tmp_dataset.data.graph_features.unsqueeze(-1) if not (cfg.Data.extra_input_columns is None) else False]):
                raise NotImplemented("Something went wrong with the data preprocessing due to differing molecular featurization of the models.")
        ## If more than 1 datalaoder, individual dataloader is required for each model
        elif len(dataloader_list) > 1:
            dataloader_list.append(dataloader_list[-1])
        previous_cfg = cfg
    
    ## Infer model
    if len(dataloader_list) > 1:
        prediction_mean, prediction_std, raw_prediction_mean, raw_prediction_std = inference.infer_ensemble(dataloader_list=dataloader_list, model_list=model_list, cfg=cfg)
    else:
        prediction_mean, prediction_std, raw_prediction_mean, raw_prediction_std = inference.infer_ensemble(dataloader=dataloader_list[0], model_list=model_list, cfg=cfg)

    # Return predictions for SMILES input via command line (only 1 input per time)
    if args.predict_data_path is None:
        for t_idx, t  in enumerate(cfg.Model.targets_to_train):
            print(f"{t} prediction: {prediction_mean[0][t_idx].item():.3f}")
        return prediction_mean.numpy()

    # Save and return predictions for csv input
    save_dir = os.path.dirname(args.predict_data_path)
    save_file_name = f"{os.path.splitext(os.path.basename(args.predict_data_path))[0]}_prediction"
    results = {
        "Prediction_results": {
            save_file_name: {
                'mol_ids': dataset.data.idx,
                'SMILES': [dataset.data[f"smiles_mol_{i}"] for i in range(len(cfg.Data.smiles_columns))],
                'extra_input': dataset.data.graph_features if not (cfg.Data.extra_input_columns is None) else None,
                'real_values': dataset.data.y,
                'raw_predictions': raw_prediction_mean,
                'predictions': prediction_mean
            }
        }
    }

    if len(model_list) > 1:
        results["Prediction_results"][save_file_name]["stds"] = prediction_std if cfg.task_type == "regression" else raw_prediction_std

    utils.save_results_to_csv(
        save_dir_results=save_dir, 
        results=results, 
        args=cfg,
        real_values=False
        )

    print(results)
    print(f"Predictions can be found at: {save_dir}/{save_file_name}.csv")
    return prediction_mean.numpy()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--predict_data_path', default=None, type=str, help="Optional: Provide path to make predictions for molecules in a CSV file.")
    parser.add_argument('--model_path', default=None, type=str, help="Provide path to trained model.")
    parser.add_argument('--config_path', default=None, type=str, help="Optional: Provide path to specific config file of trained model. Default: Config file is in same folder as model.")
    parser.add_argument('--ensemble_path', default=None, type=str, help="Optional: privde path to ensemble config. Note that model_path and config_path are not required then and will be extracted automatically from the ensemble config.")
    parser.add_argument('--SMILES', default=None, type=str)
    parser.add_argument('--extra_input', default=None, type=str, help="Optional: Additional inputs for the prediction, e.g., temperature or pressure. If multiple inputs are required, please use ';' to separate.")
    args = parser.parse_args()

    predictions = predict(args)
