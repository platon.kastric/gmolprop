import os 
import numpy as np
import torch

# hydra
import hydra
import utils.omegaconf_resolvers
from omegaconf import OmegaConf

import train_single_model
import utils.utils as utils
import utils.analyze_results as analyze_results
from source import inference

@hydra.main(version_base="1.1", config_path="./configs", config_name="Tb")
def ensemble_model_run_sequential(cfg):
    if (cfg.Data.test_path is None) and (cfg.Data.test_fraction != 0):
        raise ValueError("Test ensemble error. For training an ensemble either an overall test set needs to be defined with Data.test_path or the Data.test_fraction must be set to 0.")

    # Run single model training
    datamodules = []
    trained_models = []
    results = []
    results_metric_dict = {}
    for model_i in range(cfg.Ensemble.num_models):
        seed_i = cfg.Ensemble.seed_list[model_i]
        cfg.random_seed = seed_i
        datamodule_i, trained_models_i, results_i = train_single_model.single_model_run(cfg)
        datamodules.append(datamodule_i)
        trained_models.append(trained_models_i)
        results.append(results_i)
        for result_set, result_values in results_i["Prediction_results"].items():
            if result_set not in results_metric_dict.keys():
                results_metric_dict[result_set] = {}
            results_metric_dict[result_set][model_i+1] = result_values["metrics"]
        cfg.Ensemble.model_source_list.append(cfg.save_dir)

    # Save path
    save_dir = f"{os.path.dirname(os.path.abspath(__file__))}/{cfg.Ensemble.save_dir}"
    os.makedirs(save_dir, exist_ok=True)
    ## Save final config
    save_path = os.path.join(save_dir, "config.yaml")
    with open(save_path, "w+") as f:
        OmegaConf.save(cfg, f)

    # Analyze average single model performance
    results_dir = os.path.join(save_dir, "results")
    os.makedirs(results_dir)
    for result_set, result_set_metrics in results_metric_dict.items():
        average_single_model_metrics = analyze_results.get_average_metric(result_set_metrics)
        utils.write_metrics_to_txt(metric_dicts=average_single_model_metrics, args=cfg, path=os.path.join(results_dir, f"{result_set}_metrics_single_avg.txt"))

    # Analyse ensemble model performance (note that for current ensemble model we assume trainval in raw and test given separately)
    trainval_dataloader = datamodules[0].get_raw_dataloader()
    test_dataloader = datamodules[0].get_test_dataloader()
    model_list = [model_i["Prediction_model"] for model_i in trained_models]
    trainval_prediction_mean, trainval_prediction_std, trainval_raw_prediction_mean, trainval_raw_prediction_std = inference.infer_ensemble(dataloader=trainval_dataloader, model_list=model_list, cfg=cfg)
    if not (test_dataloader is None):
        test_prediction_mean, test_prediction_std, test_raw_prediction_mean, test_raw_prediction_std = inference.infer_ensemble(dataloader=test_dataloader, model_list=model_list, cfg=cfg)

    if cfg.task_type == "regression":
        ensemble_trainval_metric = analyze_results.calculate_regression_metrics(
            y_true = datamodules[0].raw_dataset.data.y, 
            y_pred = trainval_prediction_mean
            )
        if not (test_dataloader is None):
            ensemble_test_metric = analyze_results.calculate_regression_metrics(
                y_true = datamodules[0].raw_test_dataset.data.y, 
                y_pred = test_prediction_mean
                )
    if cfg.task_type == "classification":
        trainval_prediction_std, test_prediction_std = trainval_raw_prediction_std, test_raw_prediction_std # for classification std of class probabilities
        ensemble_trainval_metric = analyze_results.calculate_classification_metrics(
            y_true = datamodules[0].raw_dataset.data.y, 
            y_pred = trainval_prediction_mean,
            y_pred_probability = trainval_raw_prediction_mean
            )
        if not (test_dataloader is None):
            ensemble_test_metric = analyze_results.calculate_classification_metrics(
                y_true = datamodules[0].raw_test_dataset.data.y, 
                y_pred = test_prediction_mean,
                y_pred_probability = test_raw_prediction_mean
                )

    results = {
        "Prediction_results": {
            "Train_Val": {
                'metrics': ensemble_trainval_metric,
                'mol_ids': datamodules[0].raw_dataset.data.idx, 
                'SMILES': [datamodules[0].raw_dataset.data[f"smiles_mol_{i}"] for i in range(len(cfg.Data.smiles_columns))],
                'extra_input': datamodules[0].raw_dataset.data.graph_features if not (cfg.Data.extra_input_columns is None) else None,
                'raw_predictions': trainval_raw_prediction_mean,
                'real_values': datamodules[0].raw_dataset.data.y,
                'predictions': trainval_prediction_mean,
                'stds': trainval_prediction_std
            }
        }
    }
    if not (test_dataloader is None):
        results["Prediction_results"]["Test"] = {
            'metrics': ensemble_test_metric,
            'mol_ids': datamodules[0].raw_test_dataset.data.idx, 
            'SMILES': [datamodules[0].raw_test_dataset.data[f"smiles_mol_{i}"] for i in range(len(cfg.Data.smiles_columns))],
            'extra_input': datamodules[0].raw_test_dataset.data.graph_features if not (cfg.Data.extra_input_columns is None) else None,
            'raw_predictions': test_raw_prediction_mean,
            'real_values': datamodules[0].raw_test_dataset.data.y,
            'predictions': test_prediction_mean,
            'stds': test_prediction_std
        }
    
    # Save and analyze results of ensemble
    utils.save_results(
        args=cfg, 
        results=results,
        save_dir_results=results_dir
        )
    if cfg.Result_analysis.plotting:
        analyze_results.create_plots(
            args=cfg, 
            results=results["Prediction_results"],
            save_dir_results=results_dir
            )

if __name__ == "__main__":
    ensemble_model_run_sequential()
